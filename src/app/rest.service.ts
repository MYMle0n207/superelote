import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';

const headers= new HttpHeaders()
  .set('content-type', 'application/json; charset=utf-8')
  .set('Authorization', 'Basic YWUzMDkyNWQtNjNhNi00MTg5LTgzYzktZTNlYzk1ZWM2YjVh');

@Injectable({
  providedIn: 'root'
})
export class RestService {
  //Localhost
  //baseUrl:string = "http://localhost/server_superelote/superElote/";
  //Servidor
  baseUrl:string = "https://fenixio.000webhostapp.com/superElote/";
  // https://www.techiediaries.com/ionic-http-client/
  // https://reviblog.net/2020/06/27/tutorial-de-ionic-peticiones-http/
  constructor(private  httpClient : HttpClient) { }

  verIndex(modulo: string) {
    return this.httpClient.get(this.baseUrl + modulo + "/_api.php?opcion=index").toPromise();
  }
  
  accionesPOST(modulo: string, accion: string, data: any, options: any){
    return this.httpClient.post(
      this.baseUrl + modulo + "/_api.php?opcion=" + accion,
       JSON.stringify(data), options).toPromise();
  }

  SendNotification(id, mensaje: string) {
    var body = {
       app_id: '33efd6aa-e0e2-4cb8-87b0-51aafdafb0e8',
       include_player_ids: [id],
       contents: {
          en: mensaje
       },
       headings: {
          en: ""
       }
    };

    this.httpClient.post('https://onesignal.com/api/v1/notifications', body, {'headers': headers}).subscribe(data => {
           console.log(data);
      } , error => {
           console.log(error);
      });
    
  }
  //Final
}
