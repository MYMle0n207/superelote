import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RestService } from '../rest.service';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { WebsocketService } from 'src/app/websocket.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';


@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {
  
  token: any;
  total: any;
  CarritoL: any[];
  CarritoOrdenPedida: any[];
  desactivarBotonPedido = false;
  razonRechazo = 0;
  constructor(private localNotifications: LocalNotifications, public wsService: WebsocketService,public modalController: ModalController, private http: RestService, public alertController: AlertController, public toastController: ToastController) {
    this.token = localStorage.getItem('Usuario');
    let dataPost = {"token": this.token};
    this.getCarrito(dataPost);

    this.wsService.getUpdateCarrito().subscribe((msj: any) => {
      console.log("getUpdateCarrito");
      alert("getUpdateCarrito");
      console.log(msj);
      this.getCarrito(dataPost);
      this.presentProductoEliminado();
      this.localNotifications.schedule({
        id: 1,
        text: 'Ve tu carrito de compras!',
        // sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
        data: { secret: "Alo?" }
      });
    });

   

   }

   esperaUpdateCarrito(){
    this.wsService.getUpdateCarrito().subscribe((msj: any) => {
      console.log("getUpdateCarrito");
      alert("Ve tu carrito de compras");
      console.log(msj);
      this.localNotifications.schedule({
        id: 1,
        text: 'Ve tu carrito de compras!',
        // sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
        data: { secret: "Alo?" }
      });
    });
   }

  ngOnInit() {
    
  }

  eliminarProductoCarito(item){
    console.log(item);
    let dataPost = {"token": this.token};
    this.getCarrito(dataPost);
  }

  async presentAlertConfirmDisponibilidad() {
    const alert = await this.alertController.create({
      header: 'Comprar disponibilidad?',
      message: 'Al realizar esta accion te comprometes a pasar por tu pedido!',
      buttons: [
        {
          text: 'No',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.desactivarBotonPedido = false;
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            let pedido = {"token": this.token};
            //console.log(pedido);
            this.desactivarBotonPedido = true;

            // mandamos la confirmacion a la db, Cambiar dato confirmado_usuario a 1
            this.http.accionesPOST('carrito', 'AceptarCarrito', pedido, {}).then(res=>{
              //alert(JSON.stringify(res));
              console.log(res);
              console.log(res['Mensaje']);
              if(res['Mensaje']){
                console.log('Simon');
                this.pedidoRealizado();
                //localStorage.getItem('idUser');
                this.wsService.emit('NuevaOrden', {coordenadas: localStorage.getItem('idoneSignal')});
              }else{
                console.error('Error AceptarCarrito');
              }
            })
          
          }
        }
      ]
    });

    await alert.present();
  }

  
  async presentAlertEliminarProducto(item) {
    const alert = await this.alertController.create({
      header: 'EliminarProducto?',
      message: 'Esta accion no la puedes revertir!',
      buttons: [
        {
          text: 'No',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.desactivarBotonPedido = false;
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log(item.producto_id);
            console.log('Confirm Okay');
            let pedido = {"token": this.token, "data": item.producto_id};
            //console.log(pedido);
            //this.desactivarBotonPedido = true;

            // mandamos la confirmacion a la db, Cambiar dato confirmado_usuario a 1
            this.http.accionesPOST('carrito', 'EliminarProductoCarrito', pedido, {}).then(res=>{
              //alert(JSON.stringify(res));
              console.log(res);
              console.log(res['Mensaje']);
              if(res['Mensaje']){
                console.log('Simon');
                this.presentProductoEliminado();
                // Recargamos los datos
                this.getCarrito(pedido);
              }else{
                console.error('Error EliminarProductoCarrito');
              }
            })
          
          }
        }
      ]
    });

    await alert.present();
  }

  pedidoRealizado(){
    this.presentToastPedido();
    setTimeout(() => 
    {
        this.dismiss();
        this.desactivarBotonPedido = false;
    },
    2300);
  }

  async presentToastPedido(){
    const toast = await this.toastController.create({
      message: 'Pedido realizado correctamente.',
      duration: 2000
    });
    toast.present();
  }

  async presentProductoEliminado(){
    const toast = await this.toastController.create({
      message: 'Producto eliminado correctamente.',
      duration: 2000
    });
    toast.present();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  getCarrito(dataPost){
    this.http.accionesPOST('carrito', 'VerCarrito', dataPost, {}).then(res=>{
      //alert(JSON.stringify(res));
      console.log(res);
      console.log(res['Mensaje']);
      console.log(res['data']);
      
      if(res['Mensaje']){
        console.log('Simon');
        this.CarritoL = res['data'];
        this.CarritoOrdenPedida = res['PedidoPasado'];
        this.total = res['total'];
        this.razonRechazo = res['data'][0].id_razonRechazo;
      }else{
        console.error('Error carritoPOST');
      }
    });
  }

}
