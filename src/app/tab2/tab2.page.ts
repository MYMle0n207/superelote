import { Component, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { RestService } from '../rest.service';
import { WebsocketService } from 'src/app/websocket.service';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HistorialPage } from '../historial/historial.page';
import { ModalController } from '@ionic/angular';
declare var google;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  // https://ionicframework.com/docs/native/android-permissions
  // npm install @ionic-native/android-permissions

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;
  address: string;

  latitude: number;
  longitude: number;

  marker;
  coors: any;

  CoordenadasCarrito = "";
  estadoUsuario: boolean = false;
  estadoAdmin: boolean = false;

  constructor(
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private androidPermissions: AndroidPermissions,
    public alertController: AlertController,
    public toastController: ToastController,
    private http: RestService,
    private router: Router,
    public modalController: ModalController,
    public wsService: WebsocketService
    ) {
      if(localStorage.getItem('Coors')){
        localStorage.removeItem('Coors');
      }
      this.permisosAndroid();
      
  }

  ngOnInit() {
    this.comprobarEstadoUsuario();
    this.esperaUpdateCarrito();
    this.loadMap();
  }

  ionViewDidEnter() {
    this.comprobarEstadoUsuario();
    //alert(JSON.stringify(this.wsService.getUsuario()));
   // this.loadMap();
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {

      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      let coordenadas = ""+ this.latitude + "," + this.longitude +"";
      localStorage.setItem('Coors', coordenadas)

      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.addMarker();
      /*if (this.CoordenadasCarrito == null) {
        alert("Sin coordenadas del carrito");
        this.addMarker(0); 
      }else{
        alert("Con coordenadas del carrito");
        this.addMarker(1); 
      }*/
      

      this.map.addListener('dragend', () => {
        
        this.latitude = this.map.center.lat();
        this.longitude = this.map.center.lng();
        this.marker = null;
        //this.addMarker(); 
        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng());
        
      });

    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords " + lattitude + " " + longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if (value.length > 0)
            responseAddress.push(value);

        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value + ", ";
        }
        this.address = this.address.slice(0, -2);
      })
      .catch((error: any) => {
        this.address = "Address Not Available!";
      });

  }

  // Function to add marker at current location
  addMarker(){    
    console.log("Add marker works!")
    if (this.marker != null) {
      this.marker.setMap(null);  
      this.marker = null;
    }
    //alert(this.CoordenadasCarrito);
    if(this.CoordenadasCarrito == "" || this.CoordenadasCarrito == null){
      // alert("sin coordenadas");
      this.presentToastError();
       let features = [
        {
          position: this.map.getCenter(),
          icon: 'assets/icon/location-marker.png'
        },
        {
          position: new google.maps.LatLng("20.914383", "-100.744135"),
          icon: 'assets/icon/SELogo.png'
        },
        {
          position: new google.maps.LatLng("20.915550", "-100.741869"),
          icon: 'assets/icon/SELogo.png'
        }
      ];

      for (let i = 0; i < features.length; i++) {
        this.marker = new google.maps.Marker({
          position: features[i].position,
          map: this.map,
          icon: {
            url: features[i].icon,
            scaledSize: new google.maps.Size(50, 50)
          }
        });
      }
    }else{
     // alert(this.CoordenadasCarrito);
     let ubicacionCarrito = this.CoordenadasCarrito.split(",",3)
     
     // position: new google.maps.LatLng("20.916855", "-100.761420"),
      let features = [
        {
          position: this.map.getCenter(),
          icon: 'assets/icon/location-marker.png'
        },
        {
          position: new google.maps.LatLng(ubicacionCarrito[0], ubicacionCarrito[1]),
          icon: 'assets/icon/SELogo.png'
        }
      ];

      for (let i = 0; i < features.length; i++) {
        this.marker = new google.maps.Marker({
          position: features[i].position,
          map: this.map,
          icon: {
            url: features[i].icon,
            scaledSize: new google.maps.Size(50, 50)
          }
        });
      }
    }
    
   
/*
    const features = [
      {
        position: new google.maps.LatLng(20.916855, -100.761420),
        icon: 'assets/icon/location-marker.png'
      },
      {
        position: new google.maps.LatLng(20.916014, -100.762839),
        icon: 'assets/icon/SELogo.png'
      }
    ];
    
      for (let i = 0; i < features.length; i++) {
        this.marker = new google.maps.Marker({
          position: features[i].position,
          map: this.map,
          icon: {
            url: features[i].icon,
            scaledSize: new google.maps.Size(50, 50)
          }
        });
      }*/
  }

  permisosAndroid(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      (result) =>{ 
        console.log('Has permission?',result.hasPermission);
        this.androidPermissions.requestPermissions(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then((res) => {
          console.log('Pidiendo permisos');
          console.log(res);
        });
      },
      (err) => {
        console.log('error');
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION);
      }
    );
  }

  comprobarEstadoUsuario(){
    if (localStorage.getItem('Usuario')) {
      let data = {'token': localStorage.getItem('Usuario')};
      console.log(data);
      this.http.accionesPOST('login', 'CheckToken', data, {}).then(res=>{
        console.log(res);
        if(res['Mensaje']){
          this.coors = localStorage.getItem('Coors');
          localStorage.setItem('rol', res['rol']);
          if (res['rol'] == '1') {
            console.log('Usuario Admin');
            this.estadoAdmin = true;
            //Mandamos coordenas de la ubicacion del carrito.
            let data = this.coors ;
            /*this.wsService.emit('CoordenadasCarritoE', data, resp => {
              console.log(resp);
              //this.guardarStorage();
            });*/
            this.wsService.emit('CoordenadasCarritoE', data);
            this.CoordenadasCarrito = data;
            this.loadMap();
          }else{
            console.log('Usuario');
            this.estadoUsuario = true;
            this.wsService.emit('getCoordenadas', {});
            this.wsService.getCoordenadasCarrito().subscribe((msj: any) => {
              console.log("CoordenadasCarrito");
              console.log(msj);
              this.CoordenadasCarrito = msj;
              this.loadMap();
            });
          }
        }else{
          console.error('Token Invalido');
          this.wsService.emit('getCoordenadas', {});
          this.wsService.getCoordenadasCarrito().subscribe((msj: any) => {
            console.log("CorrdenadasCarrito");
            console.log(msj);
            this.CoordenadasCarrito = msj;
            this.loadMap();
          });
        }
      })
    }else{
      console.log('No existe Usuario');
      this.wsService.emit('getCoordenadas', {});
      this.wsService.getCoordenadasCarrito().subscribe((msj: any) => {
        console.log("CorrdenadasCarrito");
        console.log(msj);
        this.CoordenadasCarrito = msj;
        this.loadMap();
      });
    }
    // Aqui jalo
    //this.loadMap();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Servicio de elotes no disponible',
      message: 'Se estan cociendo los elotes y lavando el carrito... Vuelve a intentarlo mas tarde',
      buttons: [
        {
          text: 'Ok',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentToastError(){
    const toast = await this.toastController.create({
      message: '<Strong>Servicio de elotes no disponible.</>\nSe estan cociendo los elotes y lavando el carrito... Vuelve a intentarlo mas tarde',
      duration: 3000
    });
    toast.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: HistorialPage
    });
    return await modal.present();
  }

  irLogin(){
    this.router.navigate(['/login'])
  }

  irCrearCuenta(){
    this.router.navigate(['/crear-usuario'])
  }

  irDashboard(){
    this.router.navigate(['/dashboard'])
  }

  cerrarSesion(){
    localStorage.removeItem('Usuario');
    localStorage.removeItem('rol');
    this.comprobarEstadoUsuario();
  }

  esperaUpdateCarrito(){
    this.wsService.getUpdateCarrito().subscribe((msj: any) => {
      console.log("getUpdateCarrito");
      alert("Ve tu carrito de compras");
      console.log(msj);
    });
   }
}