import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CarritoPage } from '../carrito/carrito.page';
import { ProductoPage } from '../producto/producto.page';
import { RestService } from '../rest.service';
import { AlertController } from '@ionic/angular';
import { WebsocketService } from 'src/app/websocket.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  productosL: any[];
  OrdenesA: any[];
  razonRechazo: any[];
  razonRechazo_id = 0;
  estadoUsuario = false;
  estadoAdmin = false;
  estadoSeleccionRechazo = false;
  titulo = '';

  constructor(
    private localNotifications: LocalNotifications,
    public modalController: ModalController,
    private http: RestService,
    public alertController: AlertController,
    public wsService: WebsocketService
  ) {
    this.comprobarEstadoUsuario();
    this.getProductos();
    this.getCambioProductos();
    this.esperaUpdateCarrito();
  }

  ionViewDidEnter() {
    this.comprobarEstadoUsuario();
    this.getProductos();
  }

  async presentModal(data: any) {
    console.log(data);
    const modal = await this.modalController.create({
      component: ProductoPage,
      componentProps: {
        data,
      },
    });
    return await modal.present();
  }

  async presentModalCarrito() {
    const modal = await this.modalController.create({
      component: CarritoPage,
    });
    return await modal.present();
  }

  comprobarEstadoUsuario() {
    console.log('ADMIN: ' + this.estadoAdmin);
    console.log('UserL: ' + this.estadoUsuario);
    this.estadoAdmin = false;
    this.estadoUsuario = false;
    if (localStorage.getItem('Usuario')) {
      let data = { token: localStorage.getItem('Usuario') };
      console.log(data);
      this.http.accionesPOST('login', 'CheckToken', data, {}).then((res) => {
        console.log(res);
        if (res['Mensaje']) {
          localStorage.setItem('rol', res['rol']);
          if (res['rol'] == '1') {
            console.log('Usuario Admin');
            this.estadoAdmin = true;
            this.titulo = 'Pedidos';
            //Sacamos las ordenes activas.
            this.getOrdenes(data);
            this.getNuevosPedidos();
          } else {
            console.log('Usuario');
            this.estadoUsuario = true;
            this.titulo = 'Menu';
          }
        } else {
          console.error('Token Invalido');
          this.estadoUsuario = false;
          this.titulo = 'Menu';
        }
      });
    } else {
      console.log('No existe Usuario');
      this.estadoUsuario = false;
      this.titulo = 'Menu';
    }
    //this.router.navigate(['/']);
  }

  async presentAlertConfirm(item) {
    const alert = await this.alertController.create({
      header: '¿Confirmar pedido?',
      message: 'Estas seguro que contienes todo para entregar esta orden..',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'No lo tengo',
          handler: (blah) => {
            this.RechazarPedido(item);
          },
        },
        {
          text: 'Lo tengo!',
          handler: () => {
            console.log('Confirm Okay');
            let data = {
              token: localStorage.getItem('Usuario'),
              data: item.user_id,
            };
            this.http
              .accionesPOST('historial', 'actualizarPedidos', data, {})
              .then((res) => {
                console.log(res);
                if (res['Mensaje']) {
                  console.log('Pedido actualizado');
                  //Madamos la señal
                  this.wsService.emit('getUpdateCarrito', localStorage.getItem('idCliente'));
                  /*var message = { 
                    app_id: "33efd6aa-e0e2-4cb8-87b0-51aafdafb0e8",
                    contents: {"es": "Verifica tu Carrito!"},
                    included_segments: [localStorage.getItem('idCliente')]
                  };
                  this.sendNotification(message);*/
                  this.http.SendNotification(localStorage.getItem('idCliente'), "Pedido aceptado!");
                  
                  this.http
                    .accionesPOST('historial', 'VerOrdenes', data, {})
                    .then((res) => {
                      //alert(JSON.stringify(res));
                      console.log(res);
                      console.log(res['Mensaje']);
                      console.log(res['data']);

                      if (res['Mensaje']) {
                        console.log('Simon');
                        this.OrdenesA = res['data'];
                        console.log(this.OrdenesA);
                      } else {
                        console.error('Error OrdenesPOST');
                      }
                    });
                    
                } else {
                  console.error('Error al actualizar pedido');
                }
              });
          },
        },
      ],
    });
    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  desactivarProducto(item) {
    console.log(item);
    let data = {
      token: localStorage.getItem('Usuario'),
      producto_id: item.id,
      estado: item.disponible,
    };
    this.http
      .accionesPOST('productos', 'actualizarProductoDisponible', data, {})
      .then((res) => {
        //alert(JSON.stringify(res));
        console.log(res);
        console.log(res['Mensaje']);
        console.log(res['data']);

        if (res['Mensaje']) {
          console.log('Simon');
          // Avisamos del cambio
          this.cambioEnProductos();
          this.getProductos();
        } else {
          console.error('Error OrdenesPOST');
        }
      });
  }

  RechazarPedido(item) {
    console.log(item);
    console.log('Orden cancelada');
    let data = {
      token: localStorage.getItem('Usuario'),
      data: item.user_id,
      razon: 1,
    };
    this.http
      .accionesPOST('historial', 'devolverPedido', data, {})
      .then((res) => {
        console.log(res);
        if (res['Mensaje']) {
          console.log('Pedido actualizado');
          console.log('getUpdateCarrito', localStorage.getItem('idCliente'));
          this.wsService.emit('getUpdateCarrito', localStorage.getItem('idCliente'));

          /*var message = { 
            app_id: "33efd6aa-e0e2-4cb8-87b0-51aafdafb0e8",
            contents: {"es": "Verifica tu Carrito!"},
            included_segments: [localStorage.getItem('idCliente')]
          };
          this.sendNotification(message);*/
          this.http.SendNotification(localStorage.getItem('idCliente'), "Verifica tu Carrito!");

          this.getOrdenes(data);
        } else {
          console.error('Error al actualizar pedido');
        }
      });
    this.razonRechazo_id = 0;
  }

  getProductos() {
    this.http.verIndex('productos').then(
      (res: any) => {
        this.productosL = res;
        console.log(this.productosL);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  getOrdenes(data) {
    this.http.accionesPOST('historial', 'VerOrdenes', data, {}).then((res) => {
      //alert(JSON.stringify(res));
      console.log(res);
      console.log(res['Mensaje']);
      console.log(res['data']);

      if (res['Mensaje']) {
        console.log('Simon');
        this.OrdenesA = res['data'];
        console.log(this.OrdenesA);
      } else {
        console.error('Error OrdenesPOST');
      }
    });
  }

  cambioEnProductos() {
    //Esta funcion es para indicar que hubo un cambio en productos de manera general
    this.wsService.emit('CambioProductos', {});
  }

  getCambioProductos() {
    this.wsService.getActualizacionProductos().subscribe((msj: any) => {
      console.log(msj);
      this.getProductos();
    });
  }

  getNuevosPedidos() {
    let data = {
      token: localStorage.getItem('Usuario')
    };
    this.wsService.getNuevaOrden().subscribe((msj: any) => {
      console.log(msj);
      console.log(msj['coordenadas']);
      localStorage.setItem('idCliente', msj['coordenadas'])
      this.getOrdenes(data);
    });
  }

  esperaUpdateCarrito(){
    this.wsService.getUpdateCarrito().subscribe((msj: any) => {
      console.log("getUpdateCarrito");
      alert("getUpdateCarrito");
      console.log(msj);
      this.localNotifications.schedule({
        id: 1,
        text: 'Ve tu carrito de compras!',
        // sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
        data: { secret: "Alo?" }
      });
    });
   }

   /*sendNotification(data){
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Authorization": "Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj"
    };
    
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers
    };
    
    var https = require('https');
    var req = https.request(options, function(res) {  
      res.on('data', function(data) {
        console.log("Response:");
        console.log(JSON.parse(data));
      });
    });
    
    req.on('error', function(e) {
      console.log("ERROR:");
      console.log(e);
    });
    
    req.write(JSON.stringify(data));
    req.end();
   }
*/
   //Final
}
