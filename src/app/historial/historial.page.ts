import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {
  token: any;
  Cantidad = false;
  CarritoL: any[];
  constructor(public modalController: ModalController, private http: RestService) {
    this.token = localStorage.getItem('Usuario');
    let dataPost = {"token": this.token};
    this.http.accionesPOST('historial', 'VerHistorial', dataPost, {}).then(res=>{
      //alert(JSON.stringify(res));
      console.log(res);
      console.log(res['Mensaje']);
      console.log(res['data']);
      
      if(res['Mensaje']){
        console.log('Simon');
        this.CarritoL = res['dataMod'];
        
        if (res['CantidadDatos'] != 0) {
          this.Cantidad = true;
        }
      }else{
        console.error('Error carritoPOST');
      }
    })
   }

  ngOnInit() {
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
