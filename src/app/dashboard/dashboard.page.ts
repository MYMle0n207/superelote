import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle
} from 'chart.js';

Chart.register(
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle
);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  @ViewChild('lineCanvas', { static: true }) lineCanvas: ElementRef;
  @ViewChild('UserAdminsCanvas', { static: true }) UserAdminsCanvas: ElementRef;
  lineChart: any;
  UserAdminsChart: any;
  //VariablesChartProductosVendidos
  nombreChartProductosVendidos:any;
  //VariablesChartUsuariosAdmins
  nombreChartUsuariosAdmins:any;

  constructor(private router: Router,private http: RestService,) { 
    this.GetDataChartProductosVendidos();
    this.GetDataChartUsuariosAdministradores();
  }

  ngOnInit() {
  }
  
  GetDataChartProductosVendidos(){
    let data = {'token': localStorage.getItem('Usuario')};
    this.http.accionesPOST('dashboard', 'ChartProductosVendidos', data, {}).then(res=>{
      console.log(res);
      if(res['Mensaje']){
        console.log(res['nombreChart']);
        this.nombreChartProductosVendidos = res['nombreChart'];
        console.log(res['data']);
        console.log(res['labels']);
        this.lineChartMethod(res['data'], res['labels']);
      }
    })
  }

  GetDataChartUsuariosAdministradores(){
    let data = {'token': localStorage.getItem('Usuario')};
    this.http.accionesPOST('dashboard', 'ChartUsuariosAdmins', data, {}).then(res=>{
      console.log(res);
      if(res['Mensaje']){
        console.log(res['nombreChart']);
        this.nombreChartUsuariosAdmins = res['nombreChart'];
        console.log(res['data']);
        console.log(res['labels']);
        this.ChartPie(res['data'], res['labels']);
      }
    })
  }
  

  ionViewWillEnter(){
    //this.lineChartMethod();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
  /*  this.modalController.dismiss({
      'dismissed': true
    });
    */
      this.router.navigate(['/']);
  }

  lineChartMethod(dataChart, labelsChart) {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'bar',
    data: {
        labels: labelsChart,
        datasets: [{
            label: 'data',
            data: dataChart,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }
    });
  }

  ChartPie(dataChart, labelsChart) {
    this.lineChart = new Chart(this.UserAdminsCanvas.nativeElement, {
      type: 'pie',
    data: {
        labels: labelsChart,
        datasets: [{
            label: 'data',
            data: dataChart,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }]
    }
    });
  }

}
