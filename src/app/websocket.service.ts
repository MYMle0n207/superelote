import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Usuario } from 'src/app/classes/usuario';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  public socketStatus = false;
  public usuario: Usuario = null;
  constructor(private socket:Socket, private router:Router) { 
    this.checkStatus()
  }

  checkStatus(){
    this.socket.on('connect', ()=>{
      console.log('Conectado');
      this.socketStatus = true;
    });

    this.socket.on('disconnect', ()=>{
      console.log('Desconectado');
      this.socketStatus = false;
    });
  }

  emit(evento: string, payload?: any, callback?: Function){
    console.log('Emitiendo', evento);
    this.socket.emit(evento,payload,callback);
  }

  listen( evento: string){
    return this.socket.fromEvent(evento);
  }

  loginWS( nombre: string, coordenadas: any, rol: any){
    
    let data = {"nombre": nombre, "idoneSignal": localStorage.getItem('idoneSignal'), "coordenadas": coordenadas, "rol": rol};
    return new Promise((resolve, reject)=>{
      this.emit('configurar-usuario', data, resp => {
        this.usuario = new Usuario(nombre);
        console.log(resp);
        localStorage.setItem('idUser', resp.id);
        //this.guardarStorage();
      });
      resolve('Success!');
    });
  }

  logoutWS(){
    this.usuario = null;
    localStorage.removeItem('Usuario');
    const payload = {
      nombre: 'sin-nombre'
    };
    this.emit('configurar-usuario', payload, () => {});
  }

  cargarStorage(){
    if(localStorage.getItem('Usuario')){
      this.usuario = JSON.parse(localStorage.getItem('Usuario'));
      //this.loginWS(this.usuario.nombre);
    }
  }

  getCoordenadasCarrito(){
    return this.listen('coordenadas-nuevas');
  }

  getUpdateCarrito(){
    return this.listen('update-carrito');
  }

  getActualizacionProductos(){
    return this.listen('CambioProductos-Actualizado');
  }
  
  getNuevaOrden(){
    return this.listen('NuevaOrden-Actualizado');
  }

  getUsuario(){
    return this.usuario;
  }
/*
  guardarStorage(){
    localStorage.setItem('usuario', JSON.stringify(this.usuario));
  }*/
}
