import { Component, OnInit } from '@angular/core';
//import { ModalController } from '@ionic/angular';
import { RestService } from '../rest.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { WebsocketService } from 'src/app/websocket.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  
  correo: String;
  password: String;
  coors: any;
  public myForm: FormGroup | undefined;
  constructor(
   // public modalController: ModalController,
    private http: RestService,
    public alertController: AlertController,
    private router: Router,
    private fb: FormBuilder,
    public toastController: ToastController,
    public wsService: WebsocketService) { 
      this.coors = localStorage.getItem('Coors');
      this.myForm = this.fb.group({
          correo: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required]]
      });
    }

  ngOnInit() {
  }
  
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
  /*  this.modalController.dismiss({
      'dismissed': true
    });
    */
      this.router.navigate(['/']);
  }

  login(){
    // console.log(this.correo + ' '+ this.password);
    let data = {'correo': this.correo, 'password': this.password};
    console.log(data);
    this.http.accionesPOST('login', 'login', this.myForm.value, {}).then(res=>{
      //alert(JSON.stringify(res));
      console.log(res);
      console.log(res['Mensaje']);
      console.log(res['token']);
      
      if(res['Mensaje']){
        localStorage.setItem('Usuario', res['token']);
        this.wsService.checkStatus();
        
        this.wsService.loginWS(res['token'], this.coors, res['rol']).then((res)=> {
          console.log('Entrar');
          this.presentAlert();
        });
        
      }else{
        console.error('Salir');
        this.presentToastError();
      }
    })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Logeado',
      message: 'Inicio de sesion completado con exito.. ',
      buttons: [
        {
          text: 'Ok',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.dismiss();
          }
        }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentToastError(){
        const toast = await this.toastController.create({
          message: 'Error... Comprueba tus credenciales!',
          duration: 3000
        });
        toast.present();
  }
}
