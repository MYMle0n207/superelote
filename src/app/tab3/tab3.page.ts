import { Component} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HistorialPage } from '../historial/historial.page';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { WebsocketService } from 'src/app/websocket.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  darkMode: boolean = true;
  estadoUsuario: boolean = false;
  estadoAdmin: boolean = false;
  coors: any;
  constructor(
    public modalController: ModalController,
    private router: Router,
    private http: RestService,
    public wsService: WebsocketService
    ) {
    this.comprobarEstadoUsuario();
  }

  ionViewDidEnter() {
    this.comprobarEstadoUsuario();
}

  // funcion para cambiar el modo desde el toggle
  // https://www.youtube.com/watch?v=GjGcytpSDTw
  change(event) {
  if (event.detail.checked) {
      document.body.setAttribute('color-theme', 'dark');
  }else{
    document.body.setAttribute('color-theme', 'light');
  }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: HistorialPage
    });
    return await modal.present();
  }

  cerrarSesion(){
    localStorage.removeItem('Usuario');
    localStorage.removeItem('rol');
    this.comprobarEstadoUsuario();
  }

  comprobarEstadoUsuario(){
    if (localStorage.getItem('Usuario')) {
      let data = {'token': localStorage.getItem('Usuario')};
      console.log(data);
      this.http.accionesPOST('login', 'CheckToken', data, {}).then(res=>{
        console.log(res);
        if(res['Mensaje']){
          if (res['rol'] == '1') {
            this.estadoAdmin = true;
          }
          this.coors = localStorage.getItem('Coors');
          localStorage.setItem('rol', res['rol']);
          this.wsService.checkStatus();
          this.wsService.loginWS(localStorage.getItem('Usuario'), this.coors, res['rol']).then((res)=> {
            console.log(res);
            console.log('Existe Usuario');
            this.estadoUsuario = true;
          });
        }else{
          console.error('Token Invalido');
          this.estadoUsuario = false;
          this.wsService.logoutWS();
        }
      })
    }else{
      console.log('No existe Usuario');
      this.estadoUsuario = false;
    }
    //this.router.navigate(['/']);
  }

  irLogin(){
    this.router.navigate(['/login'])
  }

  irCrearCuenta(){
    this.router.navigate(['/crear-usuario'])
  }

  irDashboard(){
    this.router.navigate(['/dashboard'])
  }
}
