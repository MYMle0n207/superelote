import { Component } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
//import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private oneSignal: OneSignal,
    private alertCtrl: AlertController,
    private platform: Platform
    //,private androidPermissions: AndroidPermissions
    ) {
    this.initializeApp();
    // document.body.setAttribute('color-theme', 'dark')
  }

  initializeApp() {
    this.platform.ready().then(()=> {
        //this.changeDarkMode();
       //this.permisosAndroid();
       console.log('////////////////// SI ESTOY AQUI //////////////////////////////');
       this.setupPush();
       //console.log(this.oneSignal.getIds());
       this.oneSignal.getIds().then(data => {
        console.log(JSON.stringify(data));
        console.log(data.userId);
        localStorage.setItem('idoneSignal', data.userId);
      })
     //  var IdoneSignal = this.oneSignal.getIds();
       //console.log(IdoneSignal['__zone_symbol__value']);
     //  console.log(IdoneSignal['__zone_symbol__value']['userId']);

     });
    }

    setupPush() {
      // I recommend to put these into your environment.ts
      this.oneSignal.startInit('33efd6aa-e0e2-4cb8-87b0-51aafdafb0e8', '603478410719');
   
      //this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
   
      // Notifcation was received in general
      this.oneSignal.handleNotificationReceived().subscribe(data => {
        let msg = data.payload.body;
        let title = data.payload.title;
        let additionalData = data.payload.additionalData;
        this.showAlert(title, msg, additionalData.task);
        console.log(this.oneSignal.getIds());
      });
   
      // Notification was really clicked/opened
      this.oneSignal.handleNotificationOpened().subscribe(data => {
        // Just a note that the data is a different place here!
        let additionalData = data.notification.payload.additionalData;
   
        this.showAlert('Notification opened', 'You already read this before', additionalData.task);
      });
   
      this.oneSignal.endInit();
    }
   
    async showAlert(title, msg, task) {
      const alert = await this.alertCtrl.create({
        header: title,
        subHeader: msg,
        buttons: [
          {
            text: `Action: ${task}`,
            handler: () => {
              // E.g: Navigate to a specific screen
            }
          }
        ]
      })
      alert.present();
    }
/*     
    changeDarkMode(){
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    if (prefersDark.matches){
    document.body.classList.toggle('dark');
      }
    }

    permisosAndroid(){
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        (result) =>{ 
          console.log('Has permission?',result.hasPermission);
          this.androidPermissions.requestPermissions(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then((res) => {
            console.log('Pidiendo permisos');
            console.log(res);
          });
        },
        (err) => {
          console.log('error');
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION);
        }
      );
    }*/
    
}
