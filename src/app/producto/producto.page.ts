import { Component, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ModalController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage{
  // https://www.innobing.com/blog/ionic-framework-como-instalar-y-empezar/
  // https://como-programar.net/ionic/modals/
  // https://ionicframework.com/docs/components
  cantidadAPedir = 1;
  desactivarBotonResta = true;
  desactivarBotonSuma = false;
  desactivarBotonPedido = false;

  @Input() data: any;

  productosL: any[];
  token: any;

  constructor(public modalController: ModalController, public toastController: ToastController,public alertController: AlertController, private http: RestService) {
    console.log(this.data);
    this.token = localStorage.getItem('Usuario');
    this.http.verIndex('productos').then(
      (res: any) => {
        //this.productosL = res;
        //console.log(this.productosL)
      },
      (error) =>{
        console.error(error);
      }
    );
  }

  restarCantidad(){
    this.cantidadAPedir = this.cantidadAPedir - 1;
    this.comprobarBotones();
  }
  
  aumentarCantidad(){
    this.cantidadAPedir = this.cantidadAPedir + 1;
    this.comprobarBotones();
  }

  comprobarBotones(){
    if(this.cantidadAPedir >= 2 && this.cantidadAPedir <=6){
      this.desactivarBotonResta = false;
      this.desactivarBotonSuma = false;
    }
    if(this.cantidadAPedir == 1){
      this.desactivarBotonResta = true;
    }
    if(this.cantidadAPedir == 6){
      this.desactivarBotonSuma = true;
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Relizar pedido?',
      message: 'Estas seguro de esta compra',
      buttons: [
        {
          text: 'No',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.desactivarBotonPedido = false;
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            console.log(this.data);
            console.log(this.cantidadAPedir);
            let pedido = {"id": this.data.id, "cantidad": this.cantidadAPedir, "token": this.token};
            console.log(pedido);
            this.desactivarBotonPedido = true;
            this.desactivarBotonResta = true;
            this.desactivarBotonSuma = true;
            
            this.http.accionesPOST('productos', 'ACarrito', pedido, {}).then(res=>{
              //alert(JSON.stringify(res));
              console.log(res);
              console.log(res['Mensaje']);
              console.log(res['data']);
              console.log(res['Mensaje2']);
              if(res['Mensaje']){
                console.log('Simon');
                this.realizarPedido();
              }else{
                console.error('Error productos');
              }
            })
          }
        }
      ]
    });

    await alert.present();
  }

  realizarPedido(){
    this.presentToastPedido();
    setTimeout(() => 
    {
        this.dismiss();
    },
    2300);
  }

  async presentToastPedido(){
    const toast = await this.toastController.create({
      message: 'Pedido realizado correctamente.',
      duration: 2000
    });
    toast.present();
  }

  async presentToastError(){
    const toast = await this.toastController.create({
      message: 'No se ha pedido relizar el pedido.',
      duration: 2000
    });
    toast.present();
  }
  
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
