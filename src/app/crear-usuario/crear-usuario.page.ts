import { Component, OnInit } from '@angular/core';
import { NavController,ModalController } from '@ionic/angular';
import { RestService } from '../rest.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.page.html',
  styleUrls: ['./crear-usuario.page.scss'],
})
export class CrearUsuarioPage implements OnInit {
  correo: String;
  password: String;
  public myForm: FormGroup | undefined;
  constructor(
    public modalController: ModalController,
    private http: RestService,private router: Router,
    public alertController: AlertController,
    private fb: FormBuilder,
    private navCtrl: NavController) {
      this.myForm = this.fb.group({
         correo: ['', [Validators.required, Validators.email]],
         password: ['', [Validators.required]]
        });
     }

  ngOnInit() {
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    /*this.modalController.dismiss({
      'dismissed': true,
    });*/
    this.router.navigate(['/']);
  }

  crearCuenta(){
    // console.log(this.correo + ' '+ this.password);
    let data = {'correo': this.correo, 'password': this.password};
    console.log(data);
    this.http.accionesPOST('login', 'CrearCuenta', this.myForm.value, {}).then(res=>{
      //alert(JSON.stringify(res));
      console.log(res);
      console.log(res['Mensaje']);
      console.log(res['token']);
      if(res['Mensaje']){
        localStorage.setItem('Usuario', res['token']);
        console.log('Entrar');
        this.presentAlert();
      }else{
        console.error('Salir');
      }
    })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Cuenta Creado',
      message: 'Iniciando sesion ... ',
      buttons: [
        {
          text: 'Ok',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.dismiss();
          }
        }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

}
